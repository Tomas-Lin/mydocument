Centos Install Python
-------------------------

    install bz2

    ::

        yum install bzip2-devel

    build python 2.7.3

    ::

        wget https://www.python.org/ftp/python/2.7.3/Python-2.7.3.tgz | tar -zxvf Python-2.7.3.tgz | cd Python-2.7.3 |
        ./confugure | make | sudo make install

    Install nodejs

    ::

        wget http://nodejs.org/dist/v0.10.28/node-v0.10.28.tar.gz | tar -zxvf node-v0.10.28.tar.gz | cd node-v0.10.28 |
        ./configure | make | sudo make install

    Install 32bit MsSQL driver

    ::

        wget ftp://ftp.software.ibm.com/software/db2ii/downloads/odbc_driver/ODBC71-fs-v95-or-above-140106/isv95-or-above-odbc-Aix64-fs-140106.tar.gz

        參考資料： http://publib.boulder.ibm.com/infocenter/db2luw/v9/index.jsp?topic=%2Fcom.ibm.websphere.ii.product.install.core.doc%2Finstalling%2Fiiypisco-odbcii32b.html


