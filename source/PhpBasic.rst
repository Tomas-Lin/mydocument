PHP Basic
-----------

Error Handling function and Custom Error Hanldler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Custom Class
""""""""""""""

debug_backtrace
'''''''''''''''''

    function debug_backtrace()

        .. code-block:: php

            <?php
                //debug_trace.php
                function test($str){
                    echo "\nHi:$str";
                    var_dump(debug_backtrace());
                }

                test("friend");
            ?>

            <?php
                //real.php
                include("./debug_trace.php");
            ?>


    function debug_print_backtrace()

        .. code-block:: php

            <?php
                function a(){
                    b();
                }

                function b(){
                    c();
                }

                function c(){
                    debug_print_backtrace();
                }

                a();
            ?>

            #$: php debug_print_backtrace.php
            #
            #0  c() called at [filepath/debug_print_backtrace.php:7]
            #1  b() called at [filepath/debug_print_backtrace.php:3]
            #2  a() called at [filepath/debug_print_backtrace.php:14]

    function error_get_last()

        demo

        .. code-block:: php

            <?php
                echo $a;
                print_r(error_get_last());
            ?>
            #$ php error_get_last.php
            #PHP Notice:  Undefined variable: a in filtpath/error_get_last.php on line 2
            #Array(
            #    [type] => 8    [message] => Undefined variable: a
            #    [file] => /home/tomas/Dropbox/www/php_demo/error_get_last.php
            #    [line] => 2
            #)

    function error_log(string $message [, int $message_type = 0 [, string $destination [, string $extra_headers ]]])

        demo

        .. code-block:: php

            <?php
            Class log {
                const USER_ERROR_DIR = './log/Site_User_errors.log';
                const GENERAL_ERROR_DIR = './log/Site_General_errors.log';

                public function user($msg,$username){
                    $date = date('d.m.Y h:i:s');
                    $log = $msg."   |  Date:  ".$date."  |  User:  ".$username."\n";
                    error_log($log, 3, self::USER_ERROR_DIR);
                }


                public function general($msg){
                    $date = date('d.m.Y h:i:s');
                    $log = $msg."   |  Date:  ".$date."\n";
                    error_log($msg."   |  Tarih:  ".$date, 3, self::GENERAL_ERROR_DIR);
                }

            }
            $log = new log();
            $log->user("Error Log",'Tomass'); //use for user errors
            ?>

    Error message_type set

+---+------------------------------------------------------------+
| 0 | sent to PHP's system logger                                |
+---+------------------------------------------------------------+
| 1 | sent by email to the address in the destination parameter. |
+---+------------------------------------------------------------+
| 2 | No longer an option.                                       |
+---+------------------------------------------------------------+
| 3 | appended to the file destination                           |
+---+------------------------------------------------------------+
| 4 | sent directly to the SAPI logging handler.                 |
+---+------------------------------------------------------------+

    +------------+-----------------------------+
    | public     | Can be called by everywhere |
    +------------+-----------------------------+
    |  protected | Can be called by son class  |
    +------------+-----------------------------+
    |   private  | Can be called by self       |
    +------------+-----------------------------+

    function error_reporting()

        demo

        .. code-block:: php

            <?php
            //Turn off all error reporting
            error_reporting(0);

            //Report all PHP errors
            error_reporting(-1);

            // Report simple running errors
            error_reporting(E_ERROR | E_WARNING | E_PARSE);

            // Reporting E_NOTICE can be good too (to report uninitialized
            // variables or catch variable name misspellings ...)
            error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

            // Report all errors except E_NOTICE
            error_reporting(E_ALL & ~E_NOTICE);

            // Report all PHP errors (see changelog)
            error_reporting(E_ALL);
            ?>

    function restore_error_handler( void )

    function restore_exception_handler( void )

    function set_error_handler(  callable $error_handler [, int $error_types = E_ALL | E_STRICT ]  )

    demo

    .. code-block:: php


            <?php
            function myErrorHandler($errno, $errsstr, $errfile, $errline){
                if(!(error_reporting() && $errno)){
                    return;
                }

                switch($errno){
                case E_USER_ERROR:
                    echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
                    echo "  Fatal error on line $errline in file $errfile";
                    echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                    echo "Aborting...<br />\n";
                    exit(1);
                    break;

                case E_USER_ERROR:
                    echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
                    break;

                case E_USER_NOTICE:
                    echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
                    break;

                default:
                    echo "Unknown error type: [$errno] $errstr<br />\n";
                    break;
                }

                return true;
            }

            //function to test error handler

            function scale_by_log($vect, $scale){
                if(!is_numeric($scale) || $scale <= 0){
                    trigger_error("log(x) for x <= 0 is undefined, you used: scale = $scale", E_USER_ERROR);
                }

                if(!is_array($vect)){
                    trigger_error("Incorrect input vector, array of values expected", E_USER_WARNING);
                }

                $temp = array();

                foreach($vect as $pos=>$value){
                    if(!is_numeric($value)){
                        trigger_error("Value at position $pos is not a number, using 0 (zero)", E_USER_NOTICE);
                        $value=0;
                    }
                    $temp[$pos] = log($scale) * $value;
                }

                return $temp;
            }

            // set to the user defined error handler
            $old_error_handler = set_error_handler("myErrorHandler");

            // trigger some errors, first define a mixed array with a non-numeric item
            echo "vector a\n";
            $a = array(2, 3, "foo", 5.5, 43.3, 21.11);
            print_r($a);

            // now generate second array
            echo "----\nvector b - a notice (b = log(PI) * a)\n";
            / * Value at position $pos is not a number, using 0 (zero) * /
            $b = scale_by_log($a, M_PI);
            print_r($b);

            // this is trouble, we pass a string instead of an array
            echo "----\nvector c - a warning\n";
            / * Incorrect input vector, array of values expected * /
            $c = scale_by_log("not array", 2.3);
            var_dump($c); // NULL

            // this is a critical error, log of zero or negative number is undefined
            echo "----\nvector d - fatal error\n";
            / * log(x) for x <= 0 is undefined, you used: scale = $scale" * /
            $d = scale_by_log($a, -2.5);
            var_dump($d); // Never reached
            ?>

    function set_exception_handler( callable $exception_handler)

        demo

        .. code-block:: php

            <?php

            function exception_handler($exception){

                echo "Uncaught exception: ". $exception->getMessage() ."\n";
            }

            set_exception_handler("exception_handler");

            throw new Exception("Uncaught Exception EE");
            echo "Not Excecuted!\n";
            ?>

    function trigger_error( string $error_msg [, int $error_type = E_USER_NOTICE ] )

       demo

       .. code-block:: php

           <?php
           / *
            see set_error_handler.php
            * /
           if ($divisor == 0) {
                   trigger_error("Cannot divide by zero", E_USER_ERROR);
           }
           ?>


    不只是function，屬性也是一樣的宣告方式

    .. code-block:: php

        <?php
            class HelloWorld{
                public $name,
                    $message;
                function __construct($name, $message){
                    $this->name = $name;
                    $this->message = $message;
                }

                function __destruct(){
                    echo "這個class 被解開了";
                }

                public function callShowMessage(){
                    $this->showMessage();
                }

                private function showMessage(){
                    echo $this->name."說：".$this->message;
                }
            }

            $A = new HelloWorld('Tomas', 'HelloWorld!');
            echo $A->showMessage();
        ?>

Namespace
'''''''''''

    namespace

    demo

    .. code-block:: php

        <?php
            namespace my\name; // see "Defining Namespaces" section

            class MyClass {}
            function myfunction() {}
            const MYCONST = 1;

            $a = new MyClass;
            $c = new \my\name\MyClass; // see "Global Space" section

            $a = strlen('hi'); // see "Using namespaces: fallback to global
                               // function/constant" section

            $d = namespace\MYCONST; // see "namespace operator and __NAMESPACE__
                                    // constant" section
            $d = __NAMESPACE__ . '\MYCONST';
            echo constant($d); // see "Namespaces and dynamic language features" section
        ?>

    定義多個namespace

        .. code-block:: php

            <?php
                namespace MyProject {

                const CONNECT_OK = 1;
                class Connection { echo 'Connection'; }
                function connect() { echo 'function connect';  }
                }

                namespace { // global code
                session_start();
                $a = MyProject\connect();
                echo MyProject\Connection::start();
                }
            ?>

    使用namespace
        .. code-block:: php

            <?php
                namespace foo;

                use My\Full\Classname as Another;

                //this is the same as use My\Full\NSname as NSname

                use My\Full\NSname;

                // importing a global classuse ArrayObject;
            ?>

    Included files will default to the global namespace.

        .. code-block:: php


            <?php
                //test.php
                namespace test {
                  include 'test1.inc';
                  echo '-',__NAMESPACE__,'-<br />';
                }
            ?>

            <?php
                //test1.inc
                  echo '-',__NAMESPACE__,'-<br />';
            ?>

            Results of test.php:

            --
            -test-


參考資料
^^^^^^^^^^^^^^^^^^^^^^^^^^^

    PHP Error Handling: http://tw1.php.net/manual/en/ref.errorfunc.php
