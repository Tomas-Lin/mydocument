.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Loading Test
==================================

Locust
--------

Install
^^^^^^^^

    ::

        pip install pyzmq
        pip install locust

    simple demo

    .. code-block :: python

        from locust import HttpLocust, TaskSet, task

        class WebsiteTasks(TaskSet):

            def on_start(self):
                self.client.post("/login", {
                    "username": "test_user",
                    "password": ""
                })
            @task
            def index(self):
                self.client.get("/")
            @task
            def about(self):
                self.client.get("/about/")

        class WebsiteUser(HttpLocust):
            task_set = WebsiteTasks
            min_wait = 5000
            max_wait = 15000


    執行

    ::

        locust -f my_locustfile.py

Option
^^^^^^^

    --master-host=X.X.X.X

        設定IP，預設是127.0.0.1

    --master-port=5557

        設定Port 預設是8089

參考資料
^^^^^^^^^^^^^^^^^^^

    Document : http://docs.locust.io/en/latest/

