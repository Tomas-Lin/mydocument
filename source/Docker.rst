Docker init
=============

Install in mac
---------------

    Doc: https://docs.docker.com/installation/mac/

Install in Ubuntu
-------------------

    Doc: https://docs.docker.com/installation/ubuntulinux/

User GUI
----------

    Doc: https://docs.docker.com/userguide/

Install Nginx in Docker
------------------------

    ::

        docker run -d -p 80:80 dockerfile/nginx

Login
^^^^^^^

    ::

        boot2docker ssh
        docker login
        Username:
        Password:
        Email:



Work in image
^^^^^^^^^^^^^^^

wait

參考文件
＝＝＝＝＝

官方網站： https://www.docker.com/

簡單的練習： https://www.docker.com/tryit/
