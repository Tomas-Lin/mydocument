Git
----

Git Book
^^^^^^^^^^

版本控制
"""""""""

    記錄檔案快照，而不是差異的部份

        Git並不以此種方式儲存資料。 而是將其視為小型檔案系統的一組快照(Snapshot)。 每一次讀者提交更新時、或者儲存目前專案的狀態到Git時。 基本上它為當時的資料做一組快照並記錄參考到該快照的參考點。 為了講求效率，只要檔案沒有變更，Git不會再度儲存該檔案，而是記錄到前一次的相同檔案的連結

    大部份的操作皆可在本地端完成

    Git能檢查完整性

    Git 通常只增加資料

    三種狀態

        * 已提交(committed)

        * 已修改(modified)

        * 已暫存(staged)

初次設定Git
"""""""""""

    git confit

        /etc/gitconfig :  包含給該系統所有使用者的儲存庫使用的數值。 只要讀者傳遞 --system 參數給 git config，它就會讀取或者寫入參數到這個檔案

        ~/.gitconfig： 給讀者自己的帳號使用。 傳遞 --global 參數給 git config，它就會讀取或者寫入參數到這個檔案

        .git/config： 僅給所在的儲存庫使用。 每個階級的設定會覆寫上一層的。 因此，git/config內的設定值的優先權高過/etc/config。

    .gitignore

        設定忽略的檔案或資料夾名稱

    設定識別資料

        ::

            $ git config --global user.name "John Doe"
            $ git config --global user.email johndoe@example.com
            #指定編輯器
            $ git config --global core.editor emacs
            #指定合併工具
            $ git config --global merge.tool vimdiff
            #檢查讀者的設定
            $ git config --list

Git 基礎指令
^^^^^^^^^^^^^

git init
"""""""""

    初始化git，會產生.git 資料夾

git add
""""""""

    git add filename

    Git 開始追蹤檔案

git clone
""""""""""

    從遠端複製一份資料到本地端

    git clone url

git status
"""""""""""

    檢視檔案的狀態

git diff
"""""""""

    檢查尚未暫存的改變

    git diff --cached

    檢查已經提交的改變

git log
""""""""

    檢視提交的紀錄

        git log -p -2

            檢視最後兩次的提交

        這個選項除了顯示相同的資訊外，還另外附上每個更新的差異。 這對於重新檢視或者快速的瀏覽協同工作伙伴新增的更新非常有幫助。

        有時候用 word level 的方式比 line level 更容易看懂變化。在 git log -p 後面附加 --word-diff 選項，就可以取代預設的 line level 模式。當你在看原始碼的時候 word level 還挺有用的

        git log --stat

            若想檢視每個更新的簡略統計資訊

git commit
"""""""""""

    提交這次的所有更新

git rm
"""""""

    移除檔案並且取消檔案的追蹤

    git rm --cached

        不移除檔案，但取消檔案的追蹤

git mv
"""""""

    搬動檔案

git tag
"""""""""
    列出所有的標籤

    ::

        $ git tag -l
        #若是tag太多可以
        $ git tag -l "v1.3.2.*"

    新增標籤

    ::

        #-a 就是標籤名稱，-m 代表該標籤說明
        $ git tag -a v1.4 -m "my version 1.4"

    顯示tag 的commit 資料

    ::

        $ git show

    git push 標籤

    ::

        $ git push origin --tags

    刪除遠端的tag

    ::

        git push origin :refs/tags/my_tag

    比較現在與V2.5

    ::

        $ $ git diff v2.5 HEAD

    開一個以 v.25 當作基底的 branch

    ::

        $ git branch stable v2.5


Meld
^^^^^^

Install
""""""""

    ::

        $ sudo apt-get install meld

    官網: http://meldmerge.org/


git flow
^^^^^^^^^

Install git flow
""""""""""""""""""

    ::

        $ sudo apt-get install git-flow
        #init
        $ git flow init
        No branches exist yet.
        Base branches must be created now.
        Branch name for production releases: [master]
        Branch name for "next release"
        development:[develop]
        How to name your supporting branch prefixes?
        Feature branches? [feature/]
        Release branches? [release/]
        Hotfix branches? [hotfix/]
        Support branches? [support/]
        Version tag prefix? [] 0.0.1

    新功能或bug開發

    ::

        #開新分支
        $ git flow feature start branchname

        $ git commit -am 'XXXX'

        #開發結束
        $ git flow feature finish branchname

    push 一個分支到遠端

    ::

        $ git flow feature publish branchname
        # 或
        $ git push origin feature/branchname

    追蹤一個遠端的branch

    ::

        $ git flow feature track branchname
        #或
        $ git checkout -b feature/branchname -t origin/feature/branch

    刪除遠端的branch

    ::

        $ git push origin :featrue/branchname

    參考文章: http://ihower.tw/blog/archives/5140

    Git Book: http://git-scm.com/book/zh-tw
